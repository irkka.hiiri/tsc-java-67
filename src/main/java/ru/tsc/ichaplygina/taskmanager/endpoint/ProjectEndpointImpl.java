package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.ProjectEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.tsc.ichaplygina.taskmanager.api.ProjectEndpoint")
@RestController
@RequestMapping("/api/project")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Nullable
    @Override
    @WebMethod
    @PostMapping("/add")
    public Project add(@NotNull @WebParam(name = "project") @RequestBody final Project project) {
        return projectService.write(project);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/removeById/{id}")
    public void removeById(@NotNull @WebParam(name = "id") @PathVariable("id") final String id) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public void save(@NotNull @WebParam(name = "project") @RequestBody final Project project) {
        if (findById(project.getId()) != null) projectService.write(project);
    }
}

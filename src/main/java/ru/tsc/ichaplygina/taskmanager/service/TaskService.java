package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    public List<Task> findAll() {
        return repository.findAll();
    }

    public Task findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void remove(List<Task> tasks) {
        repository.deleteAll(tasks);
    }

    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Transactional
    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

    @Nullable
    @Transactional
    public Task write(Task task) {
        return repository.save(task);
    }

    @NotNull
    @Transactional
    public List<Task> write(@NotNull final List<Task> tasks) {
        return repository.saveAll(tasks);
    }
}

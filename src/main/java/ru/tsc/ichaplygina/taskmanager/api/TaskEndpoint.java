package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface TaskEndpoint {

    @Nullable
    @WebMethod
    @PostMapping("/add")
    Task add(@WebParam(name = "task") @RequestBody Task task);

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(@WebParam(name = "id") @PathVariable("id") String id);

    @WebMethod
    @DeleteMapping("/removeById/{id}")
    void removeById(@WebParam(name = "id") @PathVariable("id") String id);

    @WebMethod
    @PostMapping("/save")
    void save(@WebParam(name = "task") @RequestBody Task task);

}

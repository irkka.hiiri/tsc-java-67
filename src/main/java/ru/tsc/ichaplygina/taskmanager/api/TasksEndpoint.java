package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TasksEndpoint {

    @Nullable
    @WebMethod
    @PostMapping("/add")
    List<Task> add(@WebParam(name = "tasks") @RequestBody List<Task> tasks);

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @WebMethod
    @DeleteMapping("/remove")
    void remove(@WebParam(name = "tasks") @RequestBody List<Task> tasks);

    @WebMethod
    @DeleteMapping("/removeAll")
    void removeAll();

    @Nullable
    @WebMethod
    @PostMapping("/save")
    List<Task> save(@WebParam(name = "tasks") @RequestBody List<Task> tasks);

}

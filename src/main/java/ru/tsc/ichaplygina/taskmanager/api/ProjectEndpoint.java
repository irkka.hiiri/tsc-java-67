package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ProjectEndpoint {

    @Nullable
    @WebMethod
    @PostMapping("/add")
    Project add(@WebParam(name = "project") @RequestBody Project project);

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(@WebParam(name = "id") @PathVariable("id") String id);

    @WebMethod
    @DeleteMapping("/removeById/{id}")
    void removeById(@WebParam(name = "id") @PathVariable("id") String id);

    @WebMethod
    @PostMapping("/save")
    void save(@WebParam(name = "project") @RequestBody Project project);

}

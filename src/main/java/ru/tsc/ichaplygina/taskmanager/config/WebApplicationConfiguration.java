package ru.tsc.ichaplygina.taskmanager.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.tsc.ichaplygina.taskmanager.api.ProjectEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.ProjectsEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.TaskEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.TasksEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@ComponentScan("ru.tsc.ichaplygina.taskmanager")
public class WebApplicationConfiguration implements WebMvcConfigurer, WebApplicationInitializer {

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Bean
    public Endpoint projectEndpointRegistry (
            final ProjectEndpoint projectEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry (
            final TaskEndpoint taskEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectsEndpointRegistry(
            final ProjectsEndpoint projectsEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, projectsEndpoint);
        endpoint.publish("/ProjectsEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint tasksEndpointRegistry(
            final TasksEndpoint tasksEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, tasksEndpoint);
        endpoint.publish("/TasksEndpoint");
        return endpoint;
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext)
            throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamic = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamic.addMapping("/ws/*");
        dynamic.setLoadOnStartup(1);
    }


}

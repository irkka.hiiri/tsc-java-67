package ru.tsc.ichaplygina.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}

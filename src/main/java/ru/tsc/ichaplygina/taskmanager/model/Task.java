package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column(name = "created_dt")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Column(name = "completed_dt")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Column(name = "started_dt")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Nullable
    private String projectId;

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
        if (status.equals(Status.COMPLETED)) dateFinish = new Timestamp(new Date().getTime());
        if (status.equals(Status.IN_PROGRESS)) dateStart = new Timestamp(new Date().getTime());
    }

}

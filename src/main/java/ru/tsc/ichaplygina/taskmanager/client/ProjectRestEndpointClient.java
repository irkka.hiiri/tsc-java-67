package ru.tsc.ichaplygina.taskmanager.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface ProjectRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/project/";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @PostMapping("add")
    Project add(@RequestBody Project project);

    @GetMapping("findById/{id}")
    Project findById(@PathVariable("id") String id);

    @DeleteMapping("removeById/{id}")
    void removeById(@PathVariable("id") String id);

    @PutMapping("save")
    void save(@RequestBody Project project);

}

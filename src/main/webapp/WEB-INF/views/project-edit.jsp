<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit project</h1>

<form:form action="/project/edit/${project.id}/" method="post" modelAttribute="project">
    <form:input type="hidden" path="id"/>
    <p>
    <div>Name:</div>
    <div><form:input type="text" path="name"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><form:input type="text" path="description"/></div>
    </p>
    <p>
    <div>Status:</div>
    <div>
        <form:select path="status">
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    </p>
    <p>
    <div>Started on:</div>
    <div>
        <form:input type="date" path="dateStart"/>
    </div>
    </p>
    </p>
    <p>
    <div>Finished on:</div>
    <div>
        <form:input type="date" path="dateFinish"/>
    </div>
    </p>

    <button type="submit">Save</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>